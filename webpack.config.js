const webpack = require("webpack")//import dotenv from 'dotenv'
let dotenv = require("dotenv")
dotenv.config({path: __dirname + '/.env'})

module.exports = {
    entry: './src/index.js',
    output: {
        path: __dirname + '/dist',
        filename: 'bundle.js',
        publicPath: '/'
    },
    devServer: {
        contentBase: './dist'
    },
    module: {
        rules: [{
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: ['babel-loader']
        }, {
            test: /\.(css|scss)$/,
            exclude: /node_modules/,
            use: ['style-loader', 'css-loader', 'sass-loader']
        }, { 
            test: /\.css$/, 
            loader: "style-loader!css-loader" }
    ]
    },
    node: {
        fs: 'empty'
    },
    plugins: [
        new webpack.EnvironmentPlugin(['GOOGLE_MAP_KEY','DEFAULT_LATITUDE', 'DEFAULT_ZOOM','DEFAULT_LONGITUDE','GOOGLE_MAP_URL','API_BASE_URL'])
    ]
}